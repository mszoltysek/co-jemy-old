<?php

namespace Features\Application;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Symfony2Extension\Context\KernelDictionary;

/**
 * Defines application features from the specific context.
 */
class DatabaseContext implements Context, SnippetAcceptingContext
{
    use KernelDictionary;
    /**
     * @Given the database is empty
     */
    public function theDatabaseIsEmpty()
    {
        $entitiesToClear = [
            'CoreBundle:FoodSupplier',
            'CoreBundle:MenuItem'
        ];

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($entitiesToClear as $entityToClear) {
            $metaData = $em->getClassMetadata($entityToClear);
            $em->createQuery('DELETE '.$entityToClear)->execute();
            $em->getConnection()->exec('ALTER SEQUENCE ' . $metaData->getTableName().'_id_seq RESTART WITH 1');
        }
        $em->flush();
        $em->clear();
    }
}
