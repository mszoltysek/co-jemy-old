<?php

namespace Features\Application\Page\FoodSupplier;

use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

class FoodSupplierPage extends Page
{
    protected $path = '/food-suppliers/{id}/show';

    protected $elements = [
        'Food supplier menu items' => '#food-supplier-menu-items',
        'Food supplier details' => '#food-supplier-details',
        'Supplier name' => '#food-supplier-details td.food-supplier-name',
        'Supplier delivery cost' => '#food-supplier-details td.food-supplier-delivery-cost',
        'Supplier free delivery threshold' => '#food-supplier-details td.food-supplier-free-delivery-threshold',
        'Supplier single package cost' => '#food-supplier-details td.food-supplier-single-package-cost',
        'Supplier phone number' => '#food-supplier-details td.food-supplier-phone-number',
        'Supplier website url' => '#food-supplier-details td.food-supplier-website-url',
        'Supplier menu url' => 'p.food-supplier-menu-url a'
    ];

    public function hasSupplierDetails()
    {
        return $this->hasElement('Food suppliers details');
    }

    /**
     * @return bool
     */
    public function hasMenuItems()
    {
        return $this->hasElement('Food supplier menu items');
    }
    
    public function checkForMenuItems(array $menuItems)
    {
        foreach ($menuItems as $key => $parameters) {
            $name = $this
                ->getElement('Food supplier menu items')
                ->find('css', '.menu-item-'.$key)
                ->find('css', '.menu-item-name')
                ->getText();

            $price = $this
                ->getElement('Food supplier menu items')
                ->find('css', '.menu-item-'.$key)
                ->find('css', '.menu-item-price')
                ->getText();

            if ($name !== $parameters['name']) {
                throw new \RuntimeException(sprintf('Expected name %s , got %s', $parameters['name'], $name));
            }

            if ($price !== $parameters['price']) {
                throw new \RuntimeException(sprintf('Expected price %s , got %s', $parameters['price'], $price));
            }
        }
    }

    /**
     * @param array $parameters
     * @return bool
     */
    public function checkForSupplierWithParameters(array $parameters)
    {
        $expectations = [
            'name' => $this->getElement('Supplier name')->getText(),
            'deliveryCost' => $this->getElement('Supplier delivery cost')->getText(),
            'freeDeliveryThreshold' => $this->getElement('Supplier free delivery threshold')->getText(),
            'singlePackageCost' => $this->getElement('Supplier single package cost')->getText(),
            'phoneNumber' => $this->getElement('Supplier phone number')->getText(),
            'websiteUrl' => $this->getElement('Supplier website url')->getText()
        ];
        
        
        foreach ($expectations as $name => $expectation) {
            if ($expectation !== $parameters[$name]) {
                throw new \RuntimeException(sprintf('Expected %s %s , got %s', $name, $parameters[$name], $expectation));
            }
        }

        if (isset($parameters['menuUrl'])) {
            if ($this->getElement('Supplier menu url')->getAttribute('href') !== $parameters['menuUrl']) {
                throw new \RuntimeException(sprintf('Expected menuUrl %s , got %s', $parameters['menuUrl'], $this->getElement('Supplier menu url')->getAttribute('href')));
            }
        }
    }
}
