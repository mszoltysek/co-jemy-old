<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Infrastructure\CoJemy\Order\Commands\CloseOrderCommand;

class CloseOrderHandler
{
    private $eventStoreRepository;
    private $eventStorePersister;

    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    public function handleCloseOrderCommand(CloseOrderCommand $command)
    {
        $aggregateId = $command->getAggregateId();
        $order = $this->eventStoreRepository->findOrderById($aggregateId);
        $order->close();
        
        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
