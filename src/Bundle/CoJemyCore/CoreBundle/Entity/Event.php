<?php

namespace Bundle\CoJemyCore\CoreBundle\Entity;

class Event
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $eventType;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var string
     */
    private $parameters;

    /**
     * @var string
     */
    private $version;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param string $aggregateId
     *
     * @return Event
     */
    public function setAggregateId($aggregateId) : Event
    {
        $this->aggregateId = $aggregateId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAggregateId() : string
    {
        return $this->aggregateId;
    }

    /**
     * @param string $eventType
     *
     * @return Event
     */
    public function setEventType($eventType) : Event
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventType() : string
    {
        return $this->eventType;
    }

    /**
     * @param \DateTime $dateCreated
     *
     * @return Event
     */
    public function setDateCreated($dateCreated) : Event
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated() : \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param string $parameters
     *
     * @return Event
     */
    public function setParameters($parameters) : Event
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return string
     */
    public function getParameters() : string
    {
        return $this->parameters;
    }

    /**
     * @param string $version
     *
     * @return Event
     */
    public function setVersion($version) : Event
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return string
     */
    public function getVersion() : string
    {
        return $this->version;
    }
}

