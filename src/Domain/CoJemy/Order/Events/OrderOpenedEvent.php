<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class OrderOpenedEvent implements Event
{
    private $aggregateId;
    private $supplierId;
    private $hashes;
    private $pricePerPackage;
    private $deliveryCost;

    public function __construct($aggregateId, $supplierId, array $hashes, $pricePerPackage, $deliveryCost)
    {
        $this->aggregateId = $aggregateId;
        $this->supplierId = $supplierId;
        $this->pricePerPackage = $pricePerPackage;
        $this->deliveryCost = $deliveryCost;
        $this->hashes = $hashes;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return 'OrderOpenedEvent';
    }

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('supplierId', $this->supplierId);
        $parameters->setParameter('pricePerPackage', $this->pricePerPackage);
        $parameters->setParameter('deliveryCost', $this->deliveryCost);
        $parameters->setParameter('hashes', $this->hashes);
        
        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return OrderOpenedEvent
     */
    public static function fromParameters(array $parameters) : OrderOpenedEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['supplierId'],
            $parameters['hashes'],
            $parameters['pricePerPackage'],
            $parameters['deliveryCost']
        );
    }
}
